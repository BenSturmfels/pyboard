from machine import Pin
from dht import DHT22

dht_pin=Pin('GP4', Pin.OPEN_DRAIN)
dht_pin(1)

temp, hum = DHT22(dht_pin)
# temp = temp * 9 // 5 + 320   # uncomment for Fahrenheit
temp_str = '{}.{}'.format(temp//10,temp%10)
hum_str = '{}.{}'.format(hum//10,hum%10)

# Print or upload it
print(temp_str, hum_str)
