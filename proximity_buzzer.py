import pyb
from pyb import ADC, Pin, Timer

p = Pin('X1') # X1 has TIM2, CH1
tim = Timer(2, freq=1000)
ch = tim.channel(1, Timer.PWM, pin=p)
ch.pulse_width_percent(50)

adc = ADC(Pin('X1'))

while True:
  tim.freq(adc.read() / 2)
  pyb.delay(20)
