"""Waves a serve as you tilt the PyBoard.

My servo range is about -74 to 67.

"""
from pyb import Accel, delay, Servo

servo = Servo(1)
accel = Accel()

while True:
    delay(5)
    a1 = accel.x()
    delay(5)
    a2 = accel.x()
    dela(5)
    a3 = accel.x()
    a = (a1 + a2 + a3) * 3.2 - 8
    servo.angle(a)
