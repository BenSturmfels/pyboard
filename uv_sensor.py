# main.py -- put your code here!
import pyb
from pyb import ADC, Pin

p = ADC(Pin('X1'))

b = pyb.LED(4)
g = pyb.LED(2)
y = pyb.LED(3)
r = pyb.LED(1)

ratings = [(50, 0),
(227, 1),
(318, 2),
(408, 3),
(503, 4),
(606, 5),
(696, 6),
(795, 7),
(881, 8),
(976, 9),
(1079, 10),
(1170, 11)]

b.off()
g.off()
y.off()
r.off()

while True:
    raw = p.read()
    mv = raw / 4096 * 3300
    rating = [(n, m) for (n, m) in ratings if mv < n][0][1]
    print('Read: {:4d}, {:4.1f}mV, UV Index {:2d}'.format(raw, mv, rating))

    b.off()
    g.off()
    y.off()
    r.off()

    if rating <= 2:
        b.on()
    elif rating <= 6:
        g.on()
    elif rating <= 9:
        y.on()
    else:
        r.on()
    pyb.delay(500)
